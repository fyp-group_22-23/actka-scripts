# actka scripts

## actka.sh - launcher

### depends
- bash
- [gum](https://github.com/charmbracelet/gum)

### howto

fast
```sh
curl -fsSL "https://gitlab.com/fyp-group_22-23/actka-scripts/-/raw/main/actka.sh" | bash
```

slow
- clone this repo
- `chmod +x actka.sh`
- `./actka.sh`

## mapsaver.sh

### depends
- bash
- [gum](https://github.com/charmbracelet/gum)

| currently broken
| no pretty UI wraper for now, do `rosrun map_server map_saver -f $SAVENAME` instead