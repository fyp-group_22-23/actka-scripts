#! /bin/bash

# see if gum is installed
if [ ! "$(type gum)" ]; then
	echo "gum is not installed"
	echo "get it from https://github.com/charmbracelet/gum"
	exit 1
fi

# eye candy
gum style \
	--foreground 212 --border-foreground 212 --border double \
	--align center --width 50 --margin "1 2" --padding "2 4" \
	'Actka' 'Smarter Delivery, Better Management!'

SAVEDIR=$(gum input --placeholder "where to save map (will create a new dir if none exist)")
SAVENAME=$(gum input --placeholder "what name to save map as")

mkdir -p $SAVEDIR
cd $SAVEDIR
rosrun map_server map_saver -f $SAVENAME