#! /bin/bash

# see if gum is installed
if [ ! "$(type gum)" ]; then
	echo "gum is not installed"
	echo "get it from https://github.com/charmbracelet/gum"
	exit 1
fi

# eye candy
gum style \
	--foreground 212 --border-foreground 212 --border double \
	--align center --width 50 --margin "1 2" --padding "2 4" \
	'Actka' 'Smarter Delivery, Better Management!'

cd ~/catkin_ws || exit

# source ros environment
source /opt/ros/noetic/setup.bash
source devel_isolated/setup.bash

# kill all previous ros
killall roslaunch
killall rosrun

# # start roscore
# roscore &>/dev/null &
# gum spin --spinner dot --title "starting roscore" -- sleep 3
# echo "✅ roscore"

# check if port 9090 is in use if so
# start rosbridge
if [ "$(lsof -i :9090)" ]; then
	echo "✅ rosbridge"
else
	roslaunch rosbridge_server rosbridge_websocket.launch &>/dev/null &
	gum spin --spinner dot --title "starting rosbridge" -- sleep 3
	echo "✅ rosbridge"
fi

python3 src/cartgrapher_navigation/navi_raspi_move.py &>/dev/null &
gum spin --spinner dot --title "starting navi_raspi_move" -- sleep 3
echo "✅ navi_raspi_move"

# do while loop
while true; do
	# select actka's operation mode
	echo ""
	echo "Select actka's operation mode:"
	MODE=$(gum choose "lidar" "ips" "map server" "keyboard control")
	case $MODE in
		"lidar")
			# lidar mode
			echo "lidar mode, ctrl + c to exit"
			# gum spin --spinner dot --title "fake run" -- sleep 3600
			catkin_make -DCATKIN_WHITELIST_PACKAGES="cartgrapher_navigation"
			mkdir -p devel_navigation
			cp devel/setup.bash devel_navigation/setup.bash
			source devel_navigation/setup.bash
			rm -r devel build
			rosrun cartgrapher_navigation cartgrapher_navigation.launch
			;;
		"ips")
			# ips mode
			echo "ips mode, ctrl + c to exit"
			python3 Sum_IPS/track_v3.py
			;;
		"map server")
			# map server mode
			echo "map server mode, ctrl + c to exit"
			MAP=$(ls maps/ | gum filter)
			rosrun map_server map_server "maps/$MAP/$MAP.pgm" 0.1
			;;
		"keyboard control")
			bash -c rosrun teleop_twist_keyboard teleop_twist_keyboard.py
			;;
	esac
#	clear
	echo "Exit actka? (y/n)"
	gum confirm && break
done
killall roslaunch
killall rosrun
clear
